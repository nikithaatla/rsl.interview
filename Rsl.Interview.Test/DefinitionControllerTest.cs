using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using Moq.Protected;
using NUnit.Framework;
using Rsl.Interview.Api.Controller;
using Rsl.Interview.Api.Services;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Rsl.Interview.Test
{
    public class Tests
    {
        private DefinitionsController _controller;
        private DefintionRepositoryMock _definitionRepository;
        
        [SetUp]
        public void Setup()
        {
            var _configuration = new Mock<IConfiguration>();

            var oneSectionMock = new Mock<IConfigurationSection>();
            oneSectionMock.Setup(s => s.Value).Returns("2ff253c1-c952-46df-9609-8f54a767b1e5");
            _configuration.Setup(c => c.GetSection("DictionaryApiKey"))
                .Returns(oneSectionMock.Object);

            string jsonString = "[{'meta':{'id':'interview: 1','offensive':false},'shortdef':['a formal consultation usually to evaluate qualifications(as of a prospective student or employee)','a meeting at which information is obtained(as by a reporter, television commentator, or pollster) from a person','a report or reproduction of information so obtained']}]";

            var handlerMock = new Mock<HttpMessageHandler>();
            var httpClientFactoryMock = new Mock<IHttpClientFactory>();

            handlerMock.Protected()
           .Setup<Task<HttpResponseMessage>>("SendAsync",
                                            ItExpr.IsAny<HttpRequestMessage>(),
                                              ItExpr.IsAny<CancellationToken>())          
           .ReturnsAsync(new HttpResponseMessage()
           {
               StatusCode = HttpStatusCode.OK,
               Content = new StringContent(jsonString),
           })
           .Verifiable();
           
            var httpClient = new HttpClient(handlerMock.Object)
            {
                BaseAddress = new Uri("https://dictionaryapi.com/api/v3/references/collegiate/json/"),
            };
          
            httpClientFactoryMock.Setup(x => x.CreateClient("dictionaryapi"))
                                 .Returns(httpClient);


            _definitionRepository = new DefintionRepositoryMock();
            var service = new DefinitionServices(_definitionRepository, httpClientFactoryMock.Object, _configuration.Object);
            _controller = new DefinitionsController(service);
        }

        [Test]
        public async void Get_ReturnsOkResultWithData_WithValidWord()
        {
            var response = await _controller.GetAllAsync("Interview");

            Assert.IsInstanceOf<OkObjectResult>(response.Result);
            Assert.That(_definitionRepository.SearchTermResultDTOs.Contains(response.Value));            
        }

        [Test]
        public async void Get_ReturnsBadRequestResult_WithInvalidWord()
        {
            var response = await _controller.GetAllAsync("@#");

            Assert.IsInstanceOf<BadRequestResult>(response.Result);    
        }
    }
}