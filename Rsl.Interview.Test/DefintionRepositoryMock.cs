﻿using Rsl.Interview.Data.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rsl.Interview.Test
{
   public class DefintionRepositoryMock : IDefinitionRepository
    {
        public List<SearchTermResultDTO> SearchTermResultDTOs { get; set; }

        public DefintionRepositoryMock()
        {
            SearchTermResultDTOs = new List<SearchTermResultDTO>();
            
        }

        public async Task AddDefinition(SearchTermResultDTO searchTermResultDTOItem)
        {
            SearchTermResultDTOs.Add(searchTermResultDTOItem);
        }

        public async Task<SearchTermResultDTO> GetDefinition(string searchTerm)
        {
           return SearchTermResultDTOs.Find(s => s.SearchTerm == searchTerm);    
        }
    }
}
