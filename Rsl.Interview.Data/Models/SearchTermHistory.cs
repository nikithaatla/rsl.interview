﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rsl.Interview.Data.Models
{
    public class SearchTermHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime SearchedOn { get; set; }       
        public string SearchTerm { get; set; }       
        public ICollection<SearchResultsHistory> SearchTermResultsHistories { get; set; }
    }
}
