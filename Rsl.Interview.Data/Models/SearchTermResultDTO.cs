﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Rsl.Interview.Data.Models
{
    public class SearchTermResultDTO
    {
        public string SearchTerm { get; set; }

        [JsonProperty("offensive")]
        public bool IsOffensive { get; set; }

        [JsonProperty(PropertyName = "shortDef", Required = Required.Always)]
        public List<string> Results { get; set; }

    }
}
