﻿using System.Threading.Tasks;

namespace Rsl.Interview.Data.Models
{
    public interface IDefinitionRepository
    {
        Task<SearchTermResultDTO> GetDefinition(string searchTerm);
        Task AddDefinition(SearchTermResultDTO searchTermResultDTOItem);

    }
}