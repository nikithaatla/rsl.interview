﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rsl.Interview.Data.Models
{
   public class SearchResultsHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("SearchTermHistoryId")]
        public SearchTermHistory SearchTermHistory { get; set; }
        public int SearchTermHistoryId { get; set; }
        public string Result { get; set; }

    }
}
    