﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rsl.Interview.Data.Models
{
    public class SQLDefinitionsRepository : IDefinitionRepository
    {
        private readonly AppDbContext _context;
        public SQLDefinitionsRepository(AppDbContext appDbContext)
        {
            _context = appDbContext;
        }

        public async Task<SearchTermResultDTO> GetDefinition(string searchTerm)
        {
            var definitonsResults = await (from e in _context.SearchTermHistories
                                           join d in _context.SearchResultsHistories
                                           on e.Id equals d.SearchTermHistoryId
                                           where e.SearchTerm == searchTerm
                                           select d.Result).ToListAsync();

            if (definitonsResults?.Any() ?? false)
            {
                return new SearchTermResultDTO
                {
                    SearchTerm = searchTerm,
                    Results = definitonsResults
                };
            }

            return null;
        }

        public async Task AddDefinition(SearchTermResultDTO searchTermResultDTOItem)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                SearchTermHistory searchTermHistory = new SearchTermHistory
                {
                    SearchedOn = DateTime.Now,
                    SearchTerm = searchTermResultDTOItem.SearchTerm
                };
                _context.SearchTermHistories.Add(searchTermHistory);

                await _context.SaveChangesAsync();

                foreach (var result in searchTermResultDTOItem.Results)
                {
                    var searchResultsHistory = new SearchResultsHistory
                    {
                        Result = result,
                        SearchTermHistoryId = searchTermHistory.Id
                    };
                    _context.SearchResultsHistories.Add(searchResultsHistory);
                }
                await _context.SaveChangesAsync();

                await transaction.CommitAsync();
            }
        }

    }
}
