﻿using Microsoft.EntityFrameworkCore;
using Rsl.Interview.Data.Models;

namespace Rsl.Interview.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
            Database.Migrate();
        }

        public DbSet<SearchTermHistory> SearchTermHistories { get; set; }
        public DbSet<SearchResultsHistory> SearchResultsHistories { get; set; }
    }
}
