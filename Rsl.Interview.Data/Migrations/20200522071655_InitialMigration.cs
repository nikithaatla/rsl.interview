﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Rsl.Interview.Data.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SearchTermHistories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SearchedOn = table.Column<DateTime>(nullable: false),
                    SearchTerm = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SearchTermHistories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SearchResultsHistories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SearchTermHistoryId = table.Column<int>(nullable: false),
                    Result = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SearchResultsHistories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SearchResultsHistories_SearchTermHistories_SearchTermHistoryId",
                        column: x => x.SearchTermHistoryId,
                        principalTable: "SearchTermHistories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SearchResultsHistories_SearchTermHistoryId",
                table: "SearchResultsHistories",
                column: "SearchTermHistoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SearchResultsHistories");

            migrationBuilder.DropTable(
                name: "SearchTermHistories");
        }
    }
}
