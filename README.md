# Rsl Interview
This project provides definitions of a word.
Makes an external API call to https://dictionaryapi.com/products/api-collegiate-dictionary to get definitions.


## Get Started
### Tools/Technologies Used
- .NET Core 3.1
- Visual Studio 2019
- SQL Server
- Swagger


## Technical Approach
- Initially searches database for word definition.
- If the relevant word is missing, then external API call is invoked, retrieve information and save in database.
- Show definiton to user.

Environments
------------

### Local ###

Component         | Location
---------         | --------
HttpPortal        | http://localhost:51551/swagger/index.html
HttpsPortal       | https://localhost:44378/swagger/index.html
Database Server   | (localDB)\\MSSQLLocalDB
Database          |  DefinitonDB
DictionaryApiKey  |  registered key value
DictionaryApiUrl  | "https://dictionaryapi.com/api/v3/references/collegiate/json/"

Local Testing
-------------
In Swagger, https://localhost:44378/swagger/index.html
Search Word : Interview
Request Url : https://localhost:44378/Word/Search?searchTerm=Interview

Code	Details
200	
Response body
{
  "SearchTerm": "Interview",
  "IsOffensive": false,
  "Results": [
    "a formal consultation usually to evaluate qualifications (as of a prospective student or employee)",
    "a meeting at which information is obtained (as by a reporter, television commentator, or pollster) from a person",
    "a report or reproduction of information so obtained"
  ]
}