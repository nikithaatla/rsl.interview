using System;
using System.IO;
using System.Reflection;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Rsl.Interview.Api.Services;
using Rsl.Interview.Data;
using Rsl.Interview.Data.Models;

namespace Rsl.Interview.Api
{
    public class Startup
    {
        private IConfiguration _config;
        private readonly ILogger _logger;

        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            _config = configuration;
            _logger = logger;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = Assembly.GetExecutingAssembly().GetName().Name
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);

            });

            services.AddDbContextPool<AppDbContext>(options =>
                     options.UseSqlServer(_config.GetConnectionString("DefinitionDBConnection")));

            services.AddControllers()
                   .AddJsonOptions(jsonOptions =>
                   {
                       jsonOptions.JsonSerializerOptions.PropertyNamingPolicy = null;
                   });

            services.AddScoped<IDefinitionRepository, SQLDefinitionsRepository>();
            services.AddScoped<IDefinitionServices, DefinitionServices>();

            //services.AddScoped<ILogger, >();

            services.AddHttpClient("dictionaryapi", c =>
            {
                c.BaseAddress = new Uri(_config.GetValue<string>("DictionaryApiUrl"));
                c.DefaultRequestHeaders.Add("Accept", "application/json");
            });

            services.AddAutoMapper(typeof(Startup));

            // enable Application Insights telemetry
            services.AddApplicationInsightsTelemetry();
            //_logger.LogDebug("Logging from ConfigureServices.");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseExceptionHandler(a => a.Run(async context =>
            {
                var exceptionHandlerPathFeature = context.Features.Get<IExceptionHandlerPathFeature>();
                var exception = exceptionHandlerPathFeature.Error;

                var result = JsonConvert.SerializeObject(new { error = exception.Message + exception.StackTrace });
                context.Response.ContentType = "application/json";
                await context.Response.WriteAsync(result);
            }));

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
