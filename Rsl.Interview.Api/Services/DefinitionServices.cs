﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Rsl.Interview.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Rsl.Interview.Api.Services
{
    public class DefinitionServices : IDefinitionServices
    {
        private readonly IDefinitionRepository _definitionRepository;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;
        public DefinitionServices(IDefinitionRepository definitionRepository, IHttpClientFactory httpClientFactory,
                                  IConfiguration configuration)
        {
            _definitionRepository = definitionRepository;
            _httpClientFactory = httpClientFactory;
            _configuration = configuration;
        }

        public async Task<SearchTermResultDTO> ListASync(string searchTerm)
        {
            var result = await _definitionRepository.GetDefinition(searchTerm);
            if (result == null)
            {
                var httpResponseMessage = await ApicallAsync(searchTerm);

                var searchTermResultDTO = await ValidateJSONAndGetDataAsync(httpResponseMessage);
                if (searchTermResultDTO != null)
                {
                    searchTermResultDTO.SearchTerm = searchTerm;
                    await _definitionRepository.AddDefinition(searchTermResultDTO);
                }
                return searchTermResultDTO;
            }
            return result;
        }

        private async Task<SearchTermResultDTO> ValidateJSONAndGetDataAsync(HttpResponseMessage httpResponseMessage)
        {
            if (httpResponseMessage.Content != null)
            {
                var jsonString = await httpResponseMessage.Content.ReadAsStringAsync();               

                TryParseJson<List<SearchTermResultDTO>>(jsonString, out List<SearchTermResultDTO> searchTermResultDTOList);
                if (searchTermResultDTOList?.Any() ?? false)
                {
                    return searchTermResultDTOList.FirstOrDefault();
                }

                var value = JsonConvert.DeserializeObject<List<string>>(jsonString);

                return new SearchTermResultDTO {Results = value };

            }
            return null;
        }

        private async Task<HttpResponseMessage> ApicallAsync(string searchTerm)
        {
            var key = _configuration.GetSection("DictionaryApiKey").Value;
            var client = _httpClientFactory.CreateClient("dictionaryapi");
            var request = new HttpRequestMessage(HttpMethod.Get, searchTerm + "?key=" + key);

            var response = await client.SendAsync(request);
            return response;
        }

        private bool TryParseJson<T>(string value, out T result)
        {
            bool success = true;
            var settings = new JsonSerializerSettings
            {
                Error = (sender, args) => { success = false; args.ErrorContext.Handled = true; },
                MissingMemberHandling = MissingMemberHandling.Error
            };
            result = JsonConvert.DeserializeObject<T>(value, settings);
            return success;
        }
    }
}
