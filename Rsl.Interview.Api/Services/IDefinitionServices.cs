﻿using Rsl.Interview.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rsl.Interview.Api.Services
{
   public interface IDefinitionServices
    {
        Task<SearchTermResultDTO> ListASync(string searchTerm);
        //string RetreiveDefinitions(string searchTerm);
        //bool SearchDatabase(string searchTerm);

        //string GetFromDatabase(int searchId);

        //void SaveData(string Result);

        //string ExternalDictionaryCall(Uri url);
    }
}
