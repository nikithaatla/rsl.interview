﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Rsl.Interview.Data.Models;

namespace Rsl.Interview.Api.Services
{
    public class SearchTermProfile : Profile
    {
        public SearchTermProfile()
        {
            CreateMap<SearchResultsHistory, SearchTermResultDTO>();
        }
    }
}
