﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Threading.Tasks;
using Rsl.Interview.Api.Services;
using Rsl.Interview.Data.Models;
using Microsoft.AspNetCore.Http;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Logging;

namespace Rsl.Interview.Api.Controller
{
    [ApiController]
    public class DefinitionsController : ControllerBase
    {
        private readonly IDefinitionServices _definitionServices;
        private readonly ILogger _logger;
        public DefinitionsController(IDefinitionServices definitionServices, ILogger<DefinitionsController> logger)
        {
            _definitionServices = definitionServices;
            _logger = logger;
        }

        /// <summary>
        /// Takes the word and returns definitions
        /// </summary>
        /// <param name="searchTerm">The word to search for</param>
        /// <response code="200">OK</response>
        /// <response code="204">No Content</response>
        /// <response code="400">Bad Request</response>
        /// <response code="404">Not Found</response>
        /// 
        [HttpGet]
        [Route("Word/Search")]
        [ProducesResponseType(typeof(SearchTermResultDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [Produces("application/json")]
        public async Task<ActionResult<SearchTermResultDTO>> GetAllAsync([BindRequired]string searchTerm)
        {
           // _logger.LogDebug("In GetAllAsync function");
            if (searchTerm == null || (!Regex.IsMatch(searchTerm, @"^[a-zA-Z]+$")))
            {
                return BadRequest("Word is required");
            }
            var res = await _definitionServices.ListASync(searchTerm);
            return Ok(res);

        }
    }
}